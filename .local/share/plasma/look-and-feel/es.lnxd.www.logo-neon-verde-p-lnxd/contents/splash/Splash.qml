/*
 *   Copyright 2014 Marco Martin <mart@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.5
import QtQuick.Window 2.2

Rectangle {
    id: root
    color: "#ffffff"
    
    property int stage

    onStageChanged: {
        if (stage == 1) {
            logoOpacityAnimation.from = 0;
            logoOpacityAnimation.to = 1;
            logoOpacityAnimation.running = true;
            preOpacityAnimation.from = 0;
            preOpacityAnimation.to = 1;
            preOpacityAnimation.running = true;
        }
    }

    Item {
        id: content
        anchors.fill: parent
        opacity: 1

        Image {
            id: logo
            property real size: units.gridUnit * 8
            anchors.centerIn: parent
            opacity: 0
            source: "images/kdeneon.svgz"
            sourceSize.width: parent.width - (parent.width / 5)
        }

        Image {
            id: topRect
            y: root.height / 2
            anchors.horizontalCenter: parent.horizontalCenter
            opacity: 0
            source: "images/rectangle.svg"
            sourceSize.height: size
            sourceSize.width: size
            Rectangle {
                y: parent.y - (parent.height / 0.7)
                radius: 0
                anchors.horizontalCenterOffset: 0
                color: "#cccccc"
                anchors {
//                     top: root.height
                    horizontalCenter: parent.horizontalCenter
                }
                height: 5
                width: root.width
                Rectangle {
                    id: topRectRectangle
                    radius: 0
                    anchors {
                        left: parent.left
                        top: parent.top
                        bottom: parent.bottom
                    }
                    width: (parent.width / 6) * (stage - 1)
                    color: "#3dc08d"
                    Behavior on width {
                        PropertyAnimation {
                            duration: 150
                            easing.type: Easing.InOutQuad
                        }
                    }
                }
            }
        }
        
    }
    
    OpacityAnimator {
        id: logoOpacityAnimation
        running: false
        target: logo
        from: 0
        to: 1
        duration: 1000
        easing.type: Easing.InOutQuad
    }
    
    OpacityAnimator {
        id: preOpacityAnimation
        running: false
        target: topRect
        from: 0
        to: 1
        duration: 3000
        easing.type: Easing.InOutQuad
    }

}
