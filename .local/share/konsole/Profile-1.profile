[Appearance]
ColorScheme=Linux

[Cursor Options]
CursorShape=1

[General]
Command=/bin/zsh
Name=Profile-1
Parent=FALLBACK/

[Keyboard]
KeyBindings=linux

[Terminal Features]
BlinkingCursorEnabled=true
